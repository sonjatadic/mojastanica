package com.ftninformatika.jwd.modul3.cinema.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.ftninformatika.jwd.modul3.cinema.model.Prevoznik;


@Repository
public interface PrevoznikRepository extends JpaRepository<Prevoznik, Long>{
	
	Prevoznik findOneById(Long id);
	
	List<Prevoznik> findByLinijeId(Long id);

}
