package com.ftninformatika.jwd.modul3.cinema.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.ftninformatika.jwd.modul3.cinema.model.Linija;
import com.ftninformatika.jwd.modul3.cinema.service.LinijaService;
import com.ftninformatika.jwd.modul3.cinema.service.PrevoznikService;
import com.ftninformatika.jwd.modul3.cinema.web.dto.LinijaDTO;


import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class LinijaToLinijaDto implements Converter<Linija, LinijaDTO> {



	 @Autowired
	    private LinijaToLinijaDto linijaToLinijaDto;
	   
	   @Autowired
	   private PrevoznikToPrevoznikDto prevoznikToPrevoznikDto;
	   
	   

	    @Override
	    public LinijaDTO convert(Linija linija) {
	        LinijaDTO linijaDTO = new LinijaDTO();
	        linijaDTO.setId(linija.getId());
	        linijaDTO.setBrojMesta(linija.getBrojMesta());
	        linijaDTO.setCenaKarte(linija.getCenaKarte());
	        linijaDTO.setVremePolaska(linija.getVremePolaska());
	        linijaDTO.setDestinacija(linija.getDestinacija());
	        linijaDTO.setPrevoznikDTO(prevoznikToPrevoznikDto.convert(linija.getPrevoznik()));
	        
	        return linijaDTO;
	    }

	    public List<LinijaDTO> convert(List<Linija> linije){
	        List<LinijaDTO> linijeDto = new ArrayList<>();

	        for(Linija linija : linije) {
	            linijeDto.add(convert(linija));
	        }

	        return linijeDto;

	}
	    
}

