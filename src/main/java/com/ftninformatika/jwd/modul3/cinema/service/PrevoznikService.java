package com.ftninformatika.jwd.modul3.cinema.service;

import java.util.List;

import com.ftninformatika.jwd.modul3.cinema.model.Prevoznik;

public interface PrevoznikService {
	
	Prevoznik save (Prevoznik prevoznik);
	
	List<Prevoznik> findAll();

	Prevoznik findOne(Long id);
	
	List<Prevoznik> findByLinijeId(Long id);
	
	
}
