package com.ftninformatika.jwd.modul3.cinema.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.cinema.model.Linija;
import com.ftninformatika.jwd.modul3.cinema.model.Prevoznik;
import com.ftninformatika.jwd.modul3.cinema.web.dto.PrevoznikDTO;

@Component
public class PrevoznikToPrevoznikDto implements Converter<Prevoznik, PrevoznikDTO>{
	
	


	public PrevoznikDTO convert(Prevoznik prevoznik) {
		PrevoznikDTO dto = new PrevoznikDTO();
		dto.setId(prevoznik.getId());
		dto.setAdresa(prevoznik.getAdresa());
		dto.setNaziv(prevoznik.getNaziv());
		dto.setPIB(prevoznik.getPIB());
		List<Linija> linije = new ArrayList<>(prevoznik.getLinije());
		return dto;
	}
	public List<PrevoznikDTO> convert(List<Prevoznik> prevoznici){
	    List<PrevoznikDTO> prevozniciDto = new ArrayList<>();

	    for(Prevoznik prevoznik: prevoznici) {
	        prevozniciDto.add(convert(prevoznik));
	    }

	    return prevozniciDto;
	}


}
