package com.ftninformatika.jwd.modul3.cinema.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.ftninformatika.jwd.modul3.cinema.model.Linija;
import com.ftninformatika.jwd.modul3.cinema.model.Prevoznik;
import com.ftninformatika.jwd.modul3.cinema.service.LinijaService;
import com.ftninformatika.jwd.modul3.cinema.service.PrevoznikService;
import com.ftninformatika.jwd.modul3.cinema.support.LinijaToLinijaDto;
import com.ftninformatika.jwd.modul3.cinema.support.PrevoznikDtoToPrevoznik;
import com.ftninformatika.jwd.modul3.cinema.support.PrevoznikToPrevoznikDto;
import com.ftninformatika.jwd.modul3.cinema.web.dto.LinijaDTO;
import com.ftninformatika.jwd.modul3.cinema.web.dto.PrevoznikDTO;


@RestController
@RequestMapping(value = "/api/prevoznici", produces = MediaType.APPLICATION_JSON_VALUE)
public class PrevoznikController {
	
	@Autowired
	PrevoznikService prevoznikService;
	
	@Autowired 
	private PrevoznikToPrevoznikDto toPrevoznikDto;
	
	@Autowired
	private PrevoznikDtoToPrevoznik toPrevoznik;
	
	@Autowired
	private LinijaService linijaService;
	
	@Autowired
	private LinijaToLinijaDto toLinijaDto;
	
	
	@PreAuthorize("hasRole('ADMIN')")
    @GetMapping
    public ResponseEntity<List<PrevoznikDTO>> getAll(){

        List<Prevoznik> prevoznici = prevoznikService.findAll();

        return new ResponseEntity<>(toPrevoznikDto.convert(prevoznici), HttpStatus.OK);
    }
	

    //@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
    @GetMapping("/{id}")
    public ResponseEntity<PrevoznikDTO> getOne(@PathVariable Long id){
        Prevoznik prevoznik = prevoznikService.findOne(id);

        if(prevoznik != null) {
            return new ResponseEntity<>(toPrevoznikDto.convert(prevoznik), HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    

	// @PreAuthorize("hasRole('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PrevoznikDTO> create(@Valid @RequestBody PrevoznikDTO prevoznikDTO) {
		Prevoznik prevoznik = toPrevoznik.convert(prevoznikDTO);
		Prevoznik sacuvaniPrevoznik = prevoznikService.save(prevoznik);

		return new ResponseEntity<>(toPrevoznikDto.convert(sacuvaniPrevoznik), HttpStatus.CREATED);
	}


    //@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
    @GetMapping("/{id}/linije")
    public ResponseEntity<List<LinijaDTO>> findByZanrId(@PathVariable Long id){
        List<Linija> linije = linijaService.findByPrevoznikId(id);

        return new ResponseEntity<>(toLinijaDto.convert(linije), HttpStatus.OK);
    }
}
