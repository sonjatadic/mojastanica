package com.ftninformatika.jwd.modul3.cinema.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.modul3.cinema.model.Linija;
import com.ftninformatika.jwd.modul3.cinema.repository.LinijaRepository;
import com.ftninformatika.jwd.modul3.cinema.service.LinijaService;


@Service
public class JpaLinijaService implements LinijaService {
		
	 @Autowired
	 LinijaRepository linijaRepository;
	

	

	@Override
	public Linija findOne(Long id) {
		
		return linijaRepository.findOneById(id);
	}

	@Override
	public Linija save(Linija linija) {
		
		return linijaRepository.save(linija);
	}

	@Override
	public Linija update(Linija linija) {
		return linijaRepository.save(linija);
	}

	@Override
	public Linija delete(Long id) {
		Linija linija = findOne(id);
		if(linija != null) {
			linija.getPrevoznik().getLinije().remove(linija);
			linija.setPrevoznik(null);
			linija = linijaRepository.save(linija);
			linijaRepository.delete(linija);
			return linija;
		}
		return null;
	}

	@Override
	public Page<Linija> search(String destinacija, double cenaKarte, Long prevoznikId, int pageNo) {
		if(cenaKarte == 0 && prevoznikId == 0) {
			return linijaRepository.findByDestinacijaContains(destinacija, PageRequest.of(pageNo, 2));
		}else if(prevoznikId == 0) {
			return linijaRepository.findByDestinacijaContainsAndCenaKarteLessThanEqual(destinacija, cenaKarte, PageRequest.of(pageNo, 2));
		}else if(cenaKarte == 0) {
			return linijaRepository.findByDestinacijaContainsAndPrevoznikId(destinacija, prevoznikId, PageRequest.of(pageNo, 2));		}
		
		return linijaRepository.findByDestinacijaContainsAndCenaKarteLessThanAndPrevoznikId(destinacija, cenaKarte, prevoznikId, PageRequest.of(pageNo, 2));
		
	}
	
	@Override
	public Page<Linija> findAll(int pageNo) {
		
		return linijaRepository.findAll(PageRequest.of(pageNo, 2));
	}

	@Override
	public List<Linija> findByPrevoznikId(Long id) {
		return linijaRepository.findByPrevoznikId(id);
	}

	
		
}
