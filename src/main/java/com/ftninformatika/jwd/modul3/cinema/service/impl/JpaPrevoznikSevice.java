package com.ftninformatika.jwd.modul3.cinema.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.modul3.cinema.model.Prevoznik;
import com.ftninformatika.jwd.modul3.cinema.repository.PrevoznikRepository;
import com.ftninformatika.jwd.modul3.cinema.service.PrevoznikService;

@Service
public class JpaPrevoznikSevice implements PrevoznikService {

	@Autowired 
	PrevoznikRepository prevoznikRepository;
	
	
	@Override
	public Prevoznik save(Prevoznik prevoznik) {
		return prevoznikRepository.save(prevoznik);
	}

	@Override
	public List<Prevoznik> findAll() {
		
		return prevoznikRepository.findAll();
	}

	@Override
	public Prevoznik findOne(Long id) {
		
		return prevoznikRepository.findOneById(id);
	}

	@Override
	public List<Prevoznik> findByLinijeId(Long id) {
		return prevoznikRepository.findByLinijeId(id);
	}

}
