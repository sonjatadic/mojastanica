package com.ftninformatika.jwd.modul3.cinema.web.dto;



import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

import com.ftninformatika.jwd.modul3.cinema.model.Prevoznik;
import com.sun.istack.NotNull;


public class LinijaDTO {
	
	


	private Long id;
	 
	@Positive(message = "Broj mesta ne moze biti negativna vrednost!" )
	 private int brojMesta;
	
	 private double cenaKarte;

	 private String vremePolaska;
	 
	 @NotBlank
	 @NotNull
	 private String destinacija;
	 
	
	 private PrevoznikDTO prevoznikDTO;


	public LinijaDTO() {
		super();
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public int getBrojMesta() {
		return brojMesta;
	}


	public void setBrojMesta(int brojMesta) {
		this.brojMesta = brojMesta;
	}


	public double getCenaKarte() {
		return cenaKarte;
	}


	public void setCenaKarte(double cenaKarte) {
		this.cenaKarte = cenaKarte;
	}


	public String getVremePolaska() {
		return vremePolaska;
	}


	public void setVremePolaska(String vremePolaska) {
		this.vremePolaska = vremePolaska;
	}


	public String getDestinacija() {
		return destinacija;
	}


	public void setDestinacija(String destinacija) {
		this.destinacija = destinacija;
	}


	public PrevoznikDTO getPrevoznikDTO() {
		return prevoznikDTO;
	}


	public void setPrevoznikDTO(PrevoznikDTO prevoznikDTO) {
		this.prevoznikDTO = prevoznikDTO;
	}


	

	 
	 

}
