package com.ftninformatika.jwd.modul3.cinema.support;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


import com.ftninformatika.jwd.modul3.cinema.model.Linija;

import com.ftninformatika.jwd.modul3.cinema.service.LinijaService;
import com.ftninformatika.jwd.modul3.cinema.service.PrevoznikService;

import com.ftninformatika.jwd.modul3.cinema.web.dto.LinijaDTO;


@Component
public class LinijaDtoToLinija implements Converter<LinijaDTO, Linija> {
	
	 @Autowired
	    private LinijaService linijaService;

	    @Autowired
	    private PrevoznikService prevoznikService;

	    @Override
	    public Linija convert(LinijaDTO dto) {

	        Linija entity;

	        if(dto.getId() == null) {
	            entity = new Linija();
	        }else {
	            entity = linijaService.findOne(dto.getId());
	        }

	        if(entity != null) {
	            entity.setBrojMesta(dto.getBrojMesta());
	            entity.setCenaKarte(dto.getCenaKarte());
	            entity.setVremePolaska(dto.getVremePolaska());
	            entity.setDestinacija(dto.getDestinacija());
	            entity.setPrevoznik(prevoznikService.findOne(dto.getPrevoznikDTO().getId()));
	            
	        }

	        return entity;
	    }

}
