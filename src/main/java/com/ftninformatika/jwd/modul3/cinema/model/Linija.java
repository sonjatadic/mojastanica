package com.ftninformatika.jwd.modul3.cinema.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Linija {
	
	 @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	 
	 @Column
	 private int brojMesta;
	 @Column
	 private double cenaKarte;
	 @Column
	 private String vremePolaska;
	 @Column(nullable = false)
	 private String destinacija;
	 
	 @ManyToOne
	 private Prevoznik prevoznik;
 


	public Linija() {
		super();
	}

	public Linija(int brojMesta, double cenaKarte, String vremePolaska, String destinacija, Prevoznik prevoznik) {
		super();
		this.brojMesta = brojMesta;
		this.cenaKarte = cenaKarte;
		this.vremePolaska = vremePolaska;
		this.destinacija = destinacija;
		this.prevoznik = prevoznik;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + brojMesta;
		long temp;
		temp = Double.doubleToLongBits(cenaKarte);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((destinacija == null) ? 0 : destinacija.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((prevoznik == null) ? 0 : prevoznik.hashCode());
		result = prime * result + ((vremePolaska == null) ? 0 : vremePolaska.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Linija other = (Linija) obj;
		if (brojMesta != other.brojMesta)
			return false;
		if (Double.doubleToLongBits(cenaKarte) != Double.doubleToLongBits(other.cenaKarte))
			return false;
		if (destinacija == null) {
			if (other.destinacija != null)
				return false;
		} else if (!destinacija.equals(other.destinacija))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (prevoznik == null) {
			if (other.prevoznik != null)
				return false;
		} else if (!prevoznik.equals(other.prevoznik))
			return false;
		if (vremePolaska == null) {
			if (other.vremePolaska != null)
				return false;
		} else if (!vremePolaska.equals(other.vremePolaska))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Linija [id=" + id + ", brojMesta=" + brojMesta + ", cenaKarte=" + cenaKarte + ", vremePolaska="
				+ vremePolaska + ", destinacija=" + destinacija + ", prevoznik=" + prevoznik + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getBrojMesta() {
		return brojMesta;
	}

	public void setBrojMesta(int brojMesta) {
		this.brojMesta = brojMesta;
	}

	public double getCenaKarte() {
		return cenaKarte;
	}

	public void setCenaKarte(double cenaKarte) {
		this.cenaKarte = cenaKarte;
	}

	public String getVremePolaska() {
		return vremePolaska;
	}

	public void setVremePolaska(String vremePolaska) {
		this.vremePolaska = vremePolaska;
	}

	public String getDestinacija() {
		return destinacija;
	}

	public void setDestinacija(String destinacija) {
		this.destinacija = destinacija;
	}

	public Prevoznik getPrevoznik() {
		return prevoznik;
	}

	public void setPrevoznik(Prevoznik prevoznik) {
		this.prevoznik = prevoznik;
	}

	 

}
