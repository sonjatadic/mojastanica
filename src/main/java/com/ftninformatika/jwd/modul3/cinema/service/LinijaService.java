package com.ftninformatika.jwd.modul3.cinema.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.ftninformatika.jwd.modul3.cinema.model.Linija;

public interface LinijaService {
	

	List<Linija> findByPrevoznikId(Long id);
	
	Linija findOne(Long Id);
	
	Linija save (Linija linija);
	
	Linija update (Linija linija);
	
	Linija delete (Long id);
	
	Page<Linija> search(String destinacija, double cenaKarte, Long prevoznikId, int pageNo);
	
	Page<Linija> findAll(int pageNo);

}
