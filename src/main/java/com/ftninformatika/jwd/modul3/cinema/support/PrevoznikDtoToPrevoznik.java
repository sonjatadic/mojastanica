package com.ftninformatika.jwd.modul3.cinema.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.cinema.model.Linija;
import com.ftninformatika.jwd.modul3.cinema.model.Prevoznik;
import com.ftninformatika.jwd.modul3.cinema.service.PrevoznikService;
import com.ftninformatika.jwd.modul3.cinema.web.dto.PrevoznikDTO;


@Component
public class PrevoznikDtoToPrevoznik implements Converter<PrevoznikDTO, Prevoznik> {
	
	  @Autowired
	    private PrevoznikService prevoznikService;

	    @Override
	    public Prevoznik convert(PrevoznikDTO prevoznikDto) {
	        Prevoznik prevoznik;

	        if(prevoznikDto.getId() == null){
	            prevoznik = new Prevoznik();
	        }else {
	            prevoznik = prevoznikService.findOne(prevoznikDto.getId());
	        }

	        if(prevoznik != null){
	            prevoznik.setNaziv(prevoznikDto.getNaziv());
	            prevoznik.setAdresa(prevoznikDto.getAdresa());
	            prevoznik.setPIB(prevoznikDto.getPIB());
	        }
	        return prevoznik;
	    }
	}
	


