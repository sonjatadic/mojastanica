package com.ftninformatika.jwd.modul3.cinema.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.modul3.cinema.model.Linija;
import com.ftninformatika.jwd.modul3.cinema.model.Prevoznik;
import com.ftninformatika.jwd.modul3.cinema.service.LinijaService;
import com.ftninformatika.jwd.modul3.cinema.service.PrevoznikService;
import com.ftninformatika.jwd.modul3.cinema.support.LinijaDtoToLinija;
import com.ftninformatika.jwd.modul3.cinema.support.LinijaToLinijaDto;
import com.ftninformatika.jwd.modul3.cinema.support.PrevoznikDtoToPrevoznik;
import com.ftninformatika.jwd.modul3.cinema.support.PrevoznikToPrevoznikDto;
import com.ftninformatika.jwd.modul3.cinema.web.dto.LinijaDTO;
import com.ftninformatika.jwd.modul3.cinema.web.dto.PrevoznikDTO;

@RestController
@RequestMapping(value = "/api/linije", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class LinijaController {

	@Autowired
	private LinijaService linijaService;

	@Autowired
	private PrevoznikService prevoznikService;

	@Autowired
	private PrevoznikDtoToPrevoznik toPrevoznik;

	@Autowired
	private LinijaToLinijaDto toLinijaDto;

	@Autowired
	private LinijaDtoToLinija toLinija;

	@Autowired
	private PrevoznikToPrevoznikDto toPrevoznikDto;
	
	@GetMapping
	public ResponseEntity<List<LinijaDTO>> getAll(@RequestParam (required = false, defaultValue = "") String destinacija,
			@RequestParam(required = false, defaultValue = "0") double cenaKarte,
			@RequestParam(required =  false, defaultValue = "0") Long prevoznikId,
			@RequestParam (value = "pageNo", defaultValue = "0") int pageNo){
		
		Page<Linija> linije = linijaService.search(destinacija, cenaKarte, prevoznikId, pageNo);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Total-Pages", Integer.toString(linije.getTotalPages()));
		
		return new ResponseEntity<>(toLinijaDto.convert(linije.getContent()), headers, HttpStatus.OK);
		
	}


	// @PreAuthorize("hasRole('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LinijaDTO> create(@Valid @RequestBody LinijaDTO linijaDTO) {
		Linija linija = toLinija.convert(linijaDTO);
		Linija sacuvanaLinija = linijaService.save(linija);

		return new ResponseEntity<>(toLinijaDto.convert(sacuvanaLinija), HttpStatus.CREATED);
	}

	// @PreAuthorize("hasRole('ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LinijaDTO> update(@PathVariable Long id, @Valid @RequestBody LinijaDTO linijaDTO) {

		if (!id.equals(linijaDTO.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Linija linija = toLinija.convert(linijaDTO);
		Linija sacuvanaLinija = linijaService.update(linija);

		return new ResponseEntity<>(toLinijaDto.convert(sacuvanaLinija), HttpStatus.OK);
	}

	// @PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		Linija obrisanaLinija = linijaService.delete(id);

		if (obrisanaLinija != null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	// @PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<LinijaDTO> getOne(@PathVariable Long id) {
		Linija linija = linijaService.findOne(id);

		if (linija != null) {
			return new ResponseEntity<>(toLinijaDto.convert(linija), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	 //@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
    @GetMapping("/{id}/linije")
    public ResponseEntity<List<LinijaDTO>> findByZanrId(@PathVariable Long id){
        List<Linija> linije = linijaService.findByPrevoznikId(id);

        return new ResponseEntity<>(toLinijaDto.convert(linije), HttpStatus.OK);
    }
    
    //@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
    @GetMapping("/{id}/prevoznici")
    public ResponseEntity<List<PrevoznikDTO>> findByLinijeId(@PathVariable Long id){
        List<Prevoznik> prevoznici = prevoznikService.findByLinijeId(id);

        return new ResponseEntity<>(toPrevoznikDto.convert(prevoznici), HttpStatus.OK);
    }


	

	}


