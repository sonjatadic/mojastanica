INSERT INTO adresa (id, ulica, broj) VALUES (1,'Bulevar Cara Lazara', 5);
INSERT INTO adresa (id, ulica, broj) VALUES (2, 'Dalmatinska', 7);

INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga, adresa_id)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN',1);
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga, adresa_id)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK',2);
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga, adresa_id)
			VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK',2);

INSERT INTO prevoznik (id, naziv, adresa, pib) VALUES (1, 'prevoznik1', 'adresa1', 'pib1');
INSERT INTO prevoznik (id, naziv, adresa, pib) VALUES (2, 'prevoznik2', 'adresa3', 'pib2');
INSERT INTO prevoznik (id, naziv, adresa, pib) VALUES (3, 'prevoznik3', 'adresa2', 'pib3');
INSERT INTO prevoznik (id, naziv, adresa, pib) VALUES (4, 'prevoznik4', 'adresa1', 'pib4');
INSERT INTO prevoznik (id, naziv, adresa, pib) VALUES (5, 'prevoznik5', 'adresa1', 'pib5');



INSERT INTO linija (id, broj_mesta, cena_karte, vreme_polaska, destinacija, prevoznik_id) VALUES (1, 20, 230.90, '10:30', 'Bar', 2);
INSERT INTO linija (id, broj_mesta, cena_karte, vreme_polaska, destinacija, prevoznik_id) VALUES (2, 55, 300.90, '20:00', 'Bar', 3);
INSERT INTO linija (id, broj_mesta, cena_karte, vreme_polaska, destinacija, prevoznik_id) VALUES (3, 40, 900.90, '19:30', 'NS', 1);
INSERT INTO linija (id, broj_mesta, cena_karte, vreme_polaska, destinacija, prevoznik_id) VALUES (4, 30, 100.90, '20:30', 'Bor', 4);
INSERT INTO linija (id, broj_mesta, cena_karte, vreme_polaska, destinacija, prevoznik_id) VALUES (5, 70, 230.90, '08:30', 'Vrbas', 5);
